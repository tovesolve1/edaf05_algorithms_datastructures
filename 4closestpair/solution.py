
import sys
import math as m
import time


def closest_points(P):

    px = sorted(P, key=lambda tup: tup[0]) #O(nlogn)
    py = sorted(P, key=lambda tup: tup[1]) #O(nlogn)
  
    return format(closest(px, py, N), '.6f') #O(nlogn)

def closest(px, py, n):

    if n == 2:
        p1 = px[0]
        p2 = px[1]
        return distance(p1, p2) 
    elif n == 3:
        p1 = px[0]
        p2 = px[1]
        p3 = px[2]
        a = distance(p1, p2)
        b = distance(p1, p3)
        c = distance(p2, p3)
        min_dist = min(a,b,c)
        return min_dist

    half_length = len(px) // 2
    lx = px[:half_length]
    rx = px[half_length:]
    line = px[half_length]
    ly = []
    ry = []
    for point in py: #O(n) 
        if point[0] < int(line[0]):
            ly.append(point)
        else:
            ry.append(point)

    delta_left = closest(lx, ly, half_length)
    delta_right = closest(rx, ry, n - half_length)
    delta = min(delta_left, delta_right)

    sy = []
    x_min = line[0] - delta
    x_max = line[0] + delta
    for point in py: #O(n)
        if x_min <= point[0] <= x_max: 
            sy.append(point)
    len_sy = len(sy)

    for i in range(len_sy): #O(n)
        for j in range(i+1, min(i+15, len_sy)):
            dist = distance(sy[i], sy[j])
            if dist < delta:
                delta = dist
    return delta



def distance(p1, p2):
    p1x = p1[0]
    p1y = p1[1]
    p2x = p2[0]
    p2y = p2[1]
    return m.sqrt(pow(p1x-p2x, 2) + pow(p1y-p2y,2))


# -------Main---------------
tic1 = time.perf_counter()
lines = sys.stdin.read().strip().split()
N = int(lines[0]) #number of players
P = [] #points

for i in range(1, len(lines), 2): #o(n)
    x = int(lines[i])
    y = int(lines[i+1])
    P.append((x, y))

tic2 = time.perf_counter()

print(closest_points(P))

tic3 = time.perf_counter()

#print(tic1-tic2, " ", tic1-tic3)


    







