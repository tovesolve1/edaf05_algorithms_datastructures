import sys
from collections import deque


def inv_list(l):
    out = [0]*(N+1)
    for i in range(N):
        out[l[i]] = i+1
    return out


lines = sys.stdin.read().strip().split('\n')
N = int(lines[0])
#women = [[] for _ in range(N+1)]
#men = [[] for _ in range(N+1)]
women = [deque() for _ in range(N+1)]
men = [deque() for _ in range(N+1)]


allinp = [int(x) for line in lines[1:] for x in line.split()]

for person in range(2*N):
    person_inp = allinp[person*(N+1):(person+1)*(N+1)]
    person_id = person_inp[0]
    
    if women[person_id]: 
        for x in range(1, len(person_inp)):
            men[person_id].append(person_inp[x])
    else:
        person_inv_pref_list = inv_list(person_inp[1:])
        women[person_id] = person_inv_pref_list

#print("men: ", men, "women:", women)

#p = [x for x in range(1,N+1)]
p = deque(x for x in range(1,N+1))

#pairs = [0 for x in range(0,N+1)]
pairs = deque(0 for x in range(0,N+1))

while p:
    m = p.pop()
    w = men[m].popleft()
    #print("m:", m, "w:", w)
    #men[m].pop(1)
    mw = pairs[w]
    if mw == 0:
        pairs[w]=m
    elif women[w][mw] > women[w][m]:
        pairs[w] = m
        p.append(mw)
    else:
        p.append(m)


for i in range(1,N+1):
    print(pairs[i])








