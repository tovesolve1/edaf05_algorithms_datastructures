import sys
from collections import deque
import math

import copy
import time


def ford_fulkerson(graph):

    lund = N-1
    minsk = 0

    total_flow = 0
    pred = [None for i in range(N)]

    tic4 = time.perf_counter()
   
    while BFS(minsk, lund, graph, pred): #O(m+n)
        tic5 = time.perf_counter()

        n = lund
        min_flow = math.inf
        while n != minsk: #O(n)
        
            parent = pred[n]
            new_flow = graph[parent][n]
            min_flow = min(new_flow, min_flow)
            n = parent

        total_flow += min_flow
        
        n = lund
        while n != minsk: #O(n)
            parent = pred[n]
       
            graph[parent][n] -= min_flow
            graph[n][parent] += min_flow
            n = parent
        pred = [None for i in range(N)]
    tic6 = time.perf_counter()
    #print("bfs:",tic5-tic4, "uppdatera graf:", tic6-tic5)

    return total_flow



def BFS(s,t, graph, pred): #O(m+n)
    
    if s == t:
        return True
    visited = [0 for i in range(N)]
    visited[s] = 1
    q = deque([s])

    
    while q:
        node = q.popleft()
        for neighbour, flow in enumerate(graph[node]):

            if visited[neighbour] == 0 and flow > 0:
                visited[neighbour] = 1
                q.append(neighbour)
                pred[neighbour] = node
                if neighbour == t:
                 
                    return True
    return False

    

lines = sys.stdin.read().strip().split("\n")
first_line = lines[0].split()
N = int(first_line[0]) #nbr of nodes
M = int(first_line[1]) #nbr of edges
C = int(first_line[2]) #nbr of students
P = int(first_line[3]) #number of routes

edges = []
for i in range(1, M+1):
    edges.append(lines[i].split())


routes_order = []
for i in range(M+1, P+M+1):
    routes_order.append(lines[i])

removed_routes = 0
max_flow = 0


residual_graph = [[0 for i in range(N)] for j in range(N)] #O(n^2) 
  

for edge in edges: #O(m)
    u = int(edge[0])
    v = int(edge[1])
    c = int(edge[2])
    residual_graph[u][v] = c
    residual_graph[v][u] = c

for route in routes_order:
    tic1 = time.perf_counter()
    current_edge = edges[int(route)]
    u = int(current_edge[0])
    v = int(current_edge[1])
    residual_graph[u][v] = 0
    residual_graph[v][u] = 0

    temp = copy.deepcopy(residual_graph)
    tic2 = time.perf_counter()
    flow = ford_fulkerson(temp)
    tic3 = time.perf_counter()
    #print(tic2-tic1," ", tic3-tic1)

    if flow >= C:
        max_flow = flow
        removed_routes +=1
        #print(removed_routes)
    else:
        break
print(removed_routes,max_flow)









    


