import sys
from heapq import heappush
from heapq import heappop
import time


def find(v): #O(log(n))
    p = v 
    while parent[p] != None:
        p = parent[p] 
    while parent[v] != None:
        w = parent[v]
        parent[v] = p
        v = w
    return p

def union(u, v, size): #O(log(n)) 
    u = find(u) #O(log(n))
    v = find(v) #O(log(n))

    if u==v and u!= None and v != None: #already friends
        return False

    if size[u] < size[v]:
        parent[u]=v 
    elif size[u] > size[v]:
        parent[v]=u 
    else:
        parent[v]=u 
        size[u] += 1

    return True



def kurskal(): #O(m*log(m)) = O(m*log(n))
    T = []
    totalTime =0 
    B = h #B has size m
    size =  [0 for x in range(0, len(nodes)+1)]
    while B: #O(m) 
        w, e = heappop(B)
        u = int(e[0])
        v = int(e[1])
        if union(u, v, size): #O(log(n))
            T.append(e)
            totalTime = totalTime + w
    return totalTime

tic1 = time.perf_counter()

lines = sys.stdin.read().strip().split()
N = int(lines[0]) #number of people at the event 
M = int(lines[1]) #number of pairs with times
nodes = set()
dictionary = {}
h =[] #heap with pairs 


for i in range(2, len(lines), 3): #O(m*log(n))
    u = lines[i]
    v= lines[i+1]
    w = lines[i+2]
    nodes.add(u)
    nodes.add(v)
    w = int(w)
    heappush(h, (w,(u,v))) 


parent = [None for x in range(0, len(nodes)+1)]


tic2 = time.perf_counter()

print(kurskal())

tic3 = time.perf_counter()

#print(tic2-tic1, " ", tic3-tic2)






    








        

    