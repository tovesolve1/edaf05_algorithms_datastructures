import sys
import math


def alfa(i,j):
    alfa1 = s[i-1]
    alfa2 = t[j-1]
    alfa1_index = int(characters.index(alfa1))
    alfa2_index = int(characters.index(alfa2))
    return int(matrix[alfa1_index][alfa2_index])

def make_table(s,t): #O(m*n)
    opt_table = [[0 for i in range(len(t)+1)] for j in range(len(s)+1)]
    for i in range(len(s)+1):
        opt_table[i][0] = i*delta
    
    for j in range(len(t)+1):
        opt_table[0][j] = j*delta
    
    for x in range(1, len(s)+1):
        for y in range(1, len(t)+1):
            opt_table[x][y] = max( opt_table[x-1][y]+ delta, opt_table[x][y-1] + delta, opt_table[x-1][y-1] + alfa(x,y))
    return opt_table

delta = -4

lines = sys.stdin.read().strip().split("\n")
characters = lines[0].split()
nbr_of_chars = len(characters)

matrix = []

for i in range(1,nbr_of_chars+1):
    matrix.append(lines[i].split())


nbr_of_queries = int(lines[nbr_of_chars+1])

words = []
for i in range(nbr_of_chars+2,nbr_of_chars+2+nbr_of_queries):
    words.append(lines[i].split())

for word_pair in words:
    s = word_pair[0]
    t = word_pair[1]
    table = make_table(s,t)

    new_word1 = ""
    new_word2 = ""
    m = len(s)
    n = len(t)

    while m>0 or n>0: #O(n+m)
        if m>0 and n==0:
            m-=1
            new_word1 = s[m] +new_word1
            new_word2 = "*" + new_word2
        elif n>0 and m==0:
            n -=1
            new_word1 = "*" +new_word1
            new_word2 = t[n] + new_word2
        elif table[m][n] == (table[m-1][n-1] + alfa(m,n)):
            m -= 1
            n -=1
            new_word1 = s[m] +new_word1
            new_word2 = t[n] + new_word2
        elif table[m][n]==(table[m][n-1] + delta):
            n -=1
            new_word1 = "*" +new_word1
            new_word2 = t[n] + new_word2
        else:
            m -=1
            new_word1 = s[m] +new_word1
            new_word2 = "*" + new_word2
        

    print(new_word1, new_word2)




