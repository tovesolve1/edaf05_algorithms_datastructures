import sys
import time

def isNeighbour(node, possible_neighbour):
    neigbour_list = [word for word in possible_neighbour]
    for i in range (1, len(node)):
        c = node[i]
        if c not in neigbour_list:
            return False
        else:
            neigbour_list.remove(c)

    return True

def get_neighbour_list(V):
    neighbour_dict = {}
    for node in V:
        neighbour_dict[node] = []

    for node in V:
        newV = V.copy()
        newV.remove(node)
        
        for n in newV:
            if isNeighbour(node, n):
               neighbour_dict[node].append(n)
             
    return neighbour_dict

def BFS(s,t):
    if s == t:
        print("0")
        return
    visited = {}
    for v in V:
        if v == s:
            visited[v]=1
        else:
            visited[v] = 0
    q = [s]
    pred = {}
    while q:
        v = q.pop(0)
        for w in neighbour_list[v]:
            if visited[w] == 0:
                visited[w] = 1
                q.append(w)
                pred[w] = v
                if w == t:
                    dist = distance(pred, s, t, 0)
                    print(dist)
                    return
    print("Impossible")

def distance(pred, s, t, i):
    i +=1
    if pred[t] == s:
        return i
    else:
        return distance(pred, s, pred[t], i)
    

tic1 = time.perf_counter()


lines = sys.stdin.read().strip().split()
N = int(lines[0])
Q = int(lines[1])

V = [lines[i] for i in range(2, N+2)]


neighbour_list = get_neighbour_list(V)
tic2 = time.perf_counter()

for i in range(N+2, len(lines),2):

    BFS(lines[i], lines[i+1])

tic3 = time.perf_counter()

print(tic2-tic1, " ", tic3-tic1)

            




